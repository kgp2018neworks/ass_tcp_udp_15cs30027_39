/* 
 * udpserver.c - A UDP echo server 
 * usage: udpserver <port_for_server>
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#define BUFSIZE 1024

/*
 * error - wrapper for perror
 */
void error(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char **argv) {
  int sockfd; /* socket file descriptor - an ID to uniquely identify a socket by the application program */
  int portno; /* port to listen on */
  int clientlen; /* byte size of client's address */
  struct sockaddr_in serveraddr; /* server's addr */
  struct sockaddr_in clientaddr; /* client addr */
  struct hostent *hostp; /* client host info */
  char buf[BUFSIZE]; /* message buf */
  char *hostaddrp; /* dotted decimal host addr string */
  int optval; /* flag value for setsockopt */
  int n; /* message byte size */
  char fName[BUFSIZE];
  int fSize, chunk, seq_no,pre_seq, read_return, chunk_size;
  //int filefd;
  /* 
   * check command line arguments 
   */
  if (argc != 2) {
    fprintf(stderr, "usage: %s <port_for_server>\n", argv[0]);
    exit(1);
  }
  portno = atoi(argv[1]);

  printf("Server Running at port %d.....\n", portno);

  /* 
   * socket: create the socket 
   */
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd < 0) 
    error("ERROR opening socket");

  /* setsockopt: Handy debugging trick that lets 
   * us rerun the server immediately after we kill it; 
   * otherwise we have to wait about 20 secs. 
   * Eliminates "ERROR on binding: Address already in use" error. 
   */
  optval = 1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, 
       (const void *)&optval , sizeof(int));

  /*
   * build the server's Internet address
   */
  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)portno);

  /* 
   * bind: associate the parent socket with a port 
   */
  if (bind(sockfd, (struct sockaddr *) &serveraddr, 
     sizeof(serveraddr)) < 0) 
    error("ERROR on binding");

  /* 
   * main loop: wait for a datagram, then echo it
   */
  clientlen = sizeof(clientaddr);
  while (1) {
        pre_seq = 0;
        //printf("lol babay\n");
        bzero(buf, BUFSIZE);
        n = recvfrom(sockfd, buf, BUFSIZE, 0, (struct sockaddr *) &clientaddr, &clientlen);
        if (n < 0)
            error("ERROR reading from socket");
        //printf("Yo babay: %s\n", buf);

        sscanf(buf, "Filename: %s File_size: %d Total Chunks: %d", fName, &fSize, &chunk);
        bzero(buf, BUFSIZE);
        sprintf(buf, "server_%s", fName);
        sprintf(fName, "%s", buf);
        //printf("hhh %d", fSize);

        //strcat(fName, "_server");
        printf("Filename: %s of filesize: %d being received\n", fName, fSize);
        
        bzero(buf, BUFSIZE);
        sprintf(buf, "ACK");
        n = sendto(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
        if (n < 0)
            error("ERROR writing to socket");
        //printf("Sent ack: %s\n", buf);
        FILE *filefd = fopen(fName,"wb");
        /*if (filefd == -1) {
            ("Couldn't open file");
        }*/
        do {
            bzero(buf, BUFSIZE);
            read_return = recvfrom(sockfd, buf, BUFSIZE, 0, (struct sockaddr *) &clientaddr, &clientlen);
            if (read_return == -1) {
                error("Couldn't read from socket");
            }
            sscanf(buf, "%04d%04d", &seq_no, &chunk_size);
            //printf("%s\n", )
            //printf("Packet with seq_no.: %d and chunk_size = %d received...\n", seq_no, chunk_size);
            if(seq_no == pre_seq)
            {
              bzero(buf, BUFSIZE);
              sprintf(buf, "ACK %04d", seq_no);
              n = sendto(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
              if (n < 0)
                error("ERROR writing to socket");
            }
            else{
                if (fwrite(buf+8, 1, chunk_size, filefd) == -1) {
                    error("Couldn't write to file");
                  }
                else 
                {
                 // printf("written\n");
                  //printf("Received: %s\n", buf+8);
                  bzero(buf, BUFSIZE);
                  sprintf(buf, "ACK %04d", seq_no);
                  n = sendto(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
                  if (n < 0)
                    error("ERROR writing to socket");
                }
                fSize -= chunk_size;
                pre_seq = seq_no;
                printf("fsize remain : %d read_return = %d chunk_size = %d\n", fSize, read_return, chunk_size);
            }
            if (fSize == 0) {
              fclose(filefd);
                break;
            }
        } while (read_return > 0);
        

        bzero(buf, BUFSIZE);
        sprintf(buf, "ACK_FINAL");
        n = sendto(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
        if (n < 0)
            error("ERROR writing to socket");
        //printf("%s\n", buf);
        n = recvfrom(sockfd, buf, BUFSIZE, 0, (struct sockaddr *) &clientaddr, &clientlen);
        if (n < 0)
            error("Couldn't read from socket");
        if (strcmp(buf, "ACK"))
            error("Wrong Acknowledgement received 2");
        else
            printf("Response to FIN,ACK received\n");


        //close(filefd);
  }
}
