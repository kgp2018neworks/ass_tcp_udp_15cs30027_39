/*
 * tcpclient.c - A simple TCP client
 * usage: tcpclient <host> <port>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <openssl/md5.h>

#define BUFSIZE 1024

/*
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

int main(int argc, char **argv) {
    int sockfd, portno, n, fsize, filefd, read_return, i;
    struct stat obj;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    char buf[BUFSIZE];

    /* check command line arguments */
    if (argc != 4) {
        fprintf(stderr,"usage: %s <hostname> <port> <filename>\n", argv[0]);
        exit(0);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);

    stat(argv[3], &obj);
    fsize = obj.st_size;

    if(fsize<1000000) {
        printf("File is too small. Exiting now...\n");
        exit(0);
    }

    unsigned char c[MD5_DIGEST_LENGTH];
    //int i;
    FILE *inFile = fopen (argv[3], "rb");
    MD5_CTX mdContext;
    int bytes;
    unsigned char data[1024];

    if (inFile == NULL) {
        printf ("%s can't be opened.\n", argv[3]);
        return 0;
    }

    MD5_Init (&mdContext);
    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
        MD5_Update (&mdContext, data, bytes);
    MD5_Final (c,&mdContext);
//    for(i = 0; i < MD5_DIGEST_LENGTH; i++) printf("%02x", c[i]);
//    printf (" %s\n", argv[3]);
    fclose (inFile);

    /* socket: create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);

    /* connect: create a connection with the server */
    if (connect(sockfd, &serveraddr, sizeof(serveraddr)) < 0)
        error("ERROR connecting");
    sleep(10);
    /* get message line from the user */
    bzero(buf, BUFSIZE);
    sprintf(buf, "%d %s", fsize, argv[3]);

    /* send the hello message to the server */
    n = write(sockfd, buf, strlen(buf));
    if (n < 0)
        error("ERROR writing to socket");

    /* get the server's reply */
    bzero(buf, BUFSIZE);
    n = read(sockfd, buf, BUFSIZE);


    if (strcmp(buf, "ACK")) {
        error("Server didn't acknowledge");
    }

    bzero(buf, BUFSIZE);
    filefd = open(argv[3], O_RDONLY);
    while (1) {
        bzero(buf, BUFSIZE);
        read_return = read(filefd, buf, BUFSIZE);
        i = write(sockfd, buf, read_return);
        if (i == -1) {
            error("Couldn't write to socket");
        }
        if (read_return == 0) {
            close(filefd);
            break;
        }
        if (read_return == -1) {
            error("Couldn't read from file");
        }
    }
    bzero(buf, BUFSIZE);
    n = read(sockfd, buf, BUFSIZE);
    if (strcmp(buf, c)) {
        printf("MD5 Matched\n");
    } else {
        printf("MD5 Not Matched\n");
    }
    close(sockfd);
    return 0;
}
