1. To run the server and client
* run `make` in either TCP or UDP to compile
* run `make runtcpserver` in TCP folder to run tcpserver at port 2048
* run `make runtcpclient` in TCP folder to run tcpclient and send mongoose.pdf.
* run `make runudpserver` in UDP folder to run udpserver at port 2048
* run `make runudpclient` in UDP folder to run udpclient and send sample.txt
* You can run manually both the server and the client after they have compiled.

2. Time taken to transfer the file :
* TCP : 12.497261 - 2.388387 = 10.108874
* UDP : 56.996042 - 56.569447 = 0.426595


